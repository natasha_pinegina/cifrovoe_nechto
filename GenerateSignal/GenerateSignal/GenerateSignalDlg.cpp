﻿
// GenerateSignalDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "GenerateSignal.h"
#include "GenerateSignalDlg.h"
#include "afxdialogex.h"
#include "AudioFile.h"

using namespace std;

#define KOORDGET(x,y) (xpget*((x)-xminget)),(ypget*((y)-ymaxget)) 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CGenerateSignalDlg



CGenerateSignalDlg::CGenerateSignalDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_GENERATESIGNAL_DIALOG, pParent)
	, A1(5)
	, A2(3)
	, F1(0.5)
	, F2(0.1)
	, P1(6.28)
	, P2(3.14)
	, N(30)
	,freq(1024)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGenerateSignalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_A1, A1);
	DDX_Text(pDX, IDC_A2, A2);
	DDX_Text(pDX, IDC_F1, F1);
	DDX_Text(pDX, IDC_F2, F2);
	DDX_Text(pDX, IDC_P1, P1);
	DDX_Text(pDX, IDC_P2, P2);
	DDX_Text(pDX, IDC_N, N);
	DDX_Text(pDX, IDC_N2, freq);
}

BEGIN_MESSAGE_MAP(CGenerateSignalDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CGenerateSignalDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// Обработчики сообщений CGenerateSignalDlg

BOOL CGenerateSignalDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	PicWnd_Signal = GetDlgItem(IDC_Signal);
	PicDc_Signal = PicWnd_Signal->GetDC();
	PicWnd_Signal->GetClientRect(&Pic_Signal);

	PicWnd_Freq = GetDlgItem(IDC_FREQ);
	PicDc_Freq = PicWnd_Freq->GetDC();
	PicWnd_Freq->GetClientRect(&Pic_Freq);

	graf_pen2.CreatePen(PS_SOLID, 2, RGB(220, 20, 0));

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CGenerateSignalDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CGenerateSignalDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CGenerateSignalDlg::SignalR(double t)
{
	double result = 0;
	double Amplitude[] = { A1, A2};
	double Phase[] = { P1, P2};
	double W[] = { F1, F2};
	for (int i = 0; i < 1; i++)
	{
		//result += Amplitude[i] * sin(W[i] * t + Phase[i]);
		result += Amplitude[i] * sin(IzmFreq(t) * t + Phase[i]);
	}
	return result;
}

void CGenerateSignalDlg::DrawGraph(std::vector<std::vector<double>>& Mass, double MinX, double MaxX, std::vector<CPen*> GraphPen, CDC* WinDc, CRect WinPic)
{
	//----- поиск максимального и минимального значения -----------------------------

	vector<double> MaxY;
	MaxY.resize(Mass.size());

	vector<double> MinY;
	MinY.resize(Mass.size());

	for (int i = 0; i < Mass.size(); i++)
	{
		for (int j = 0; j < Mass[i].size(); j++)
		{
			if (MaxY[i] < Mass[i][j]) MaxY[i] = Mass[i][j];
			if (MinY[i] > Mass[i][j]) MinY[i] = Mass[i][j];
		}
	}


	xminget = MinX;			//минимальное значение х
	xmaxget = MaxX;			//максимальное значение х
	yminget = -1;			//минимальное значение y
	ymaxget = 0;			//максимальное значение y

	for (int i = 0; i < MaxY.size(); i++)
	{
		if (MaxY[i] > ymaxget) ymaxget = MaxY[i];
		if (MinY[i] < yminget) yminget = MinY[i];
	}

	yminget = -ymaxget;

	xpget = ((double)(WinPic.Width()) / (xmaxget - xminget));			//Коэффициенты пересчёта координат по Х
	ypget = -((double)(WinPic.Height()) / (ymaxget - yminget));

	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	MemDc->FillSolidRect(WinPic, RGB(255, 255, 255));

	MemDc->SelectObject(&setka_pen);
	double shagX = (xmaxget - xminget) / 10;
	//отрисовка сетки по y
	for (float x = xminget; x <= xmaxget; x += shagX)
	{
		MemDc->MoveTo(KOORDGET(x, ymaxget));
		MemDc->LineTo(KOORDGET(x, yminget));
	}
	double shagY = (ymaxget - yminget) / 10;
	//отрисовка сетки по x
	for (float y = yminget; y <= ymaxget; y += shagY)
	{
		MemDc->MoveTo(KOORDGET(xminget, y));
		MemDc->LineTo(KOORDGET(xmaxget, y));
	}
	
	MemDc->SelectObject(&osi_pen);
	//создаём Ось Y
	//MemDc->MoveTo(KOORDGET(0, ymaxget));
	//MemDc->LineTo(KOORDGET(0, yminget));
	//создаём Ось Х
	MemDc->MoveTo(KOORDGET(xminget, 0));
	MemDc->LineTo(KOORDGET(xmaxget, 0));

	//подпись осей
	MemDc->TextOutW(KOORDGET(0, ymaxget - 0.2), _T("")); //Y
	MemDc->TextOutW(KOORDGET(xmaxget - 0.3, 0 - 0.2), _T("A")); //X

	CFont font3;
	font3.CreateFontW(15, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	MemDc->SelectObject(font3);
	//по Y с шагом 5
	for (double i = yminget; i <= ymaxget; i += shagY)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		MemDc->TextOutW(KOORDGET(0.01, i), str);
	}
	
	for (double j = xminget; j <= xmaxget; j += shagX)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		MemDc->TextOutW(KOORDGET(j, -0.00005), str);
	}
	double shag = (MaxX - MinX) / Mass[0].size();
	MemDc->SelectObject(&graf_pen2);
	for (int i = 0; i < Mass.size(); i++)
	{
		double x = MinX;
		MemDc->MoveTo(KOORDGET(MinX, Mass[i][0]));
		for (int j = 0; j < Mass[i].size(); j++)
		{
			MemDc->LineTo(KOORDGET(x, Mass[i][j]));
			x += shag;
		}
	}

	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

double CGenerateSignalDlg::IzmFreq(double t)
{
	return /*100*t*/5;
}

void CGenerateSignalDlg::OnBnClickedButton1()
{
	UpdateData(true);
	vector<double> Signal;
	vector<double> Freq;
	double sampling_period = 1 / freq;
	Signal.resize(N / sampling_period);
	Freq.resize(N / sampling_period);
	int iter = 0;
	for (double i = kol_otsh; i < kol_otsh + N; i+= sampling_period)
	{
		Signal[iter] = SignalR(i);
		Freq[iter] = IzmFreq(i);
		iter++;
	}
	vector<vector<double>>spectr;
	spectr.resize(1);
	spectr[0] = Signal;



	AudioFile<double> audioFile;
	AudioFile<double>::AudioBuffer buffer;
	buffer.resize(2);
	buffer[0].resize(N / sampling_period);
	buffer[1].resize(N / sampling_period);
	int numChannels = 2;
	int numSamplesPerChannel = 100000;
	float sampleRate = 44100.f;
	float frequency = 440.f;
	int iterr = 0;
	for (double i = kol_otsh; i < kol_otsh + N; i += sampling_period)
	{
		float sample = (float)spectr[0][iterr];
		

		for (int channel = 0; channel < numChannels; channel++)
			buffer[channel][iterr] = sample*0.5;
		iterr++;
	}

	bool ok = audioFile.setAudioBuffer(buffer);
	audioFile.save("audioFile.wav", AudioFileFormat::Wave);

	vector<vector<double>>spectr2;
	spectr2.resize(1);
	spectr2[0] = Freq;
	
	DrawGraph(spectr, kol_otsh, kol_otsh + N, GraphPen, PicDc_Signal, Pic_Signal);
	DrawGraph(spectr2, kol_otsh, kol_otsh + N, GraphPen, PicDc_Freq, Pic_Freq);
	kol_otsh += N;
}
