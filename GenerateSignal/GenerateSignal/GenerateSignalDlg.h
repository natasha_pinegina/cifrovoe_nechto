﻿
// GenerateSignalDlg.h: файл заголовка
//

#pragma once
#include <vector>

// Диалоговое окно CGenerateSignalDlg
class CGenerateSignalDlg : public CDialogEx
{
// Создание
public:
	CGenerateSignalDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GENERATESIGNAL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double A1;
	double A2;
	double F1;
	double F2;
	double P1;
	double P2;
	double N;

	CWnd* PicWnd_Signal;
	CDC* PicDc_Signal;
	CRect Pic_Signal;

	double SignalR(double t);
	
	void DrawGraph(std::vector<std::vector<double>>& Mass, double MinX, double MaxX, std::vector<CPen*> GraphPen, CDC* WinDc, CRect WinPic);

	double xminget, xmaxget,
		   yminget, ymaxget,
		   xpget, ypget;

	CPen osi_pen;		// для осей 
	CPen setka_pen;		// для сетки
	CPen graf_pen;		// для графика функции
	CPen graf_pen2;
	CPen graf_pen3;
	afx_msg void OnBnClickedButton1();
	double kol_otsh;
	std::vector<CPen*> GraphPen;
	double freq;
	double CGenerateSignalDlg::IzmFreq(double t);

	CWnd* PicWnd_Freq;
	CDC* PicDc_Freq;
	CRect Pic_Freq;
};
