﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется GenerateSignal.rc
//
#define IDD_GENERATESIGNAL_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_A1                          1000
#define IDC_A2                          1001
#define IDC_F1                          1002
#define IDC_F2                          1003
#define IDC_P1                          1004
#define IDC_P2                          1005
#define IDC_N                           1006
#define IDC_BUTTON1                     1007
#define IDC_Signal                      1008
#define IDC_N2                          1009
#define IDC_FREQ                        1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
