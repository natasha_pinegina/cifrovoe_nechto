﻿
// GenerateSignal.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CGenerateSignalApp:
// Сведения о реализации этого класса: GenerateSignal.cpp
//

class CGenerateSignalApp : public CWinApp
{
public:
	CGenerateSignalApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CGenerateSignalApp theApp;
